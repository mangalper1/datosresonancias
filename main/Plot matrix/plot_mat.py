import scipy.io
from heatmap import *
import seaborn as sb
import sys
def network(x):
    x = x[0].replace(".", " ").split("networks ")[1]
    if "(L)" in x or "(R)" in x:
        a =  x.split("(")
        x = a[0] + " (" + a[1]
    else:
        x = x.split("(")[0]
    return x

def atlas(x):
    return x[0].split(".")[1]


path = sys.argv[1]
num_vert= int(sys.argv[2])
num_vert1= int(sys.argv[3])
dic=scipy.io.loadmat(path)
matz=dic['Z'][num_vert:num_vert1,num_vert:num_vert1]
num=len(dic['names'][0])

names_list = [network(a) if 'networks' in a[0] else atlas(a) for a in dic['names'].reshape(num,).tolist() ]




sb.set(font_scale=0.5)
heat_map = sb.heatmap(matz)
cbar = heat_map.collections[0].colorbar
cbar.ax.tick_params(labelsize=10)
heat_map.set_xticklabels(names_list[num_vert:num_vert1],rotation=45,horizontalalignment='right')
heat_map.set_yticklabels(names_list[num_vert:num_vert1], rotation=0)

plt.show()

