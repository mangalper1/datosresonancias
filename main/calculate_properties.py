import scipy.io
import igraph
#from glpk import *
import json
import pickle
from auxiliar import *
import numpy as np
import time
from joblib import Parallel, delayed



def createGraph(name, matrix, threshold, edgeWeights):
    """Creating graph from matrix"""
    f = open(name+".net","w+")
    f.write("*Vertices "+str(len(matrix))+"\n")
    f.write("*Edges\n")
    for i in range(1,len(matrix)):
        for j in range(i+1, len(matrix)):
            if(matrix[i][j]>=threshold):
                f.write(str(i)+" "+str(j)+" "+str(matrix[i][j])+"\n")
                edgeWeights.append(matrix[i][j])
                #edges with value 0 as correlation coefficient are ignored
    return edgeWeights
            



def calculatingValuesWithWeights(gp, eW,size):
    community = gp.community_optimal_modularity()
    comm_members=community.membership
    max_commMember=max(comm_members)

    betweenness=gp.betweenness(weights=eW)#“betweenness” o “intermediación”, que mide el número de conversaciones (o caminos más cortos entre nodos aleatorios) que potencialmente pueden pasar por un nodo de la red.
  
    modularity= gp.modularity(community.membership,gp.es["weight"])

    averagePathLength = gp.average_path_length() # Es la media del numero de pasos de los caminos mas cortos para todos los pares posibles
    cliqueNumber = gp.clique_number() # Todos los subgrafos completos que están dentro del grafo
    degree = gp.degree() # El grado de todos los vertices
    maxDegree = np.max(degree) # El grado maximo
    minDegree = np.min(degree) # El grado minimo
    avgDegree = np.average(degree) # La media de grados
    varDegree = np.var(degree) # La varianza del grado
    density = gp.density() # Relacion entre el numero de aristas y el numero de aristas maximo
    
    
    eigenvectorCentrality_weighted = gp.eigenvector_centrality(eW)
    transivityUndirected = gp.transitivity_undirected("zero") #Es la propabilidad de que dos vecinos estén conectados


    return { "Average Path Length": averagePathLength, "Clique Number": cliqueNumber,
            "Degree": degree, "Maximum Degree": maxDegree,"Minimum Degree": minDegree,
            "Average of Degree": avgDegree,
            "Variance of Degree":varDegree, "Density": density,
            "Eigenvector Centrality": eigenvectorCentrality_weighted,
            "Eigenvector Centrality avg":np.average(eigenvectorCentrality_weighted),
            "Eigenvector Centrality min":np.min(eigenvectorCentrality_weighted),
            "Eigenvector Centrality max":np.max(eigenvectorCentrality_weighted),
            "Eigenvecor Centrality variance": np.var(eigenvectorCentrality_weighted),
            "Undirected Transivity": transivityUndirected,"Community":community,
            "Max community membership": max_commMember,"Community members":comm_members,
            "Modularity": modularity, 
            "Betweenness": betweenness, "Betweenness max":np.max(betweenness),"Betweenness min":np.min(betweenness),
            "Betweenness average": np.average(betweenness), "Betweenness variance": np.var(betweenness) }






def calculate_data_from_multiple_matrix(dirs,name='res_analysis' ,threshold = 0.8):
    res_dic = {}
    def calc_data(dir,res_dic, threshold):
        start = time.time()
        name_subject = format_dir(dir)
        # Loading matrix
        dic = scipy.io.loadmat(dir)
        matz = abs(dic['Z'][:, :164])
	    # getting all vertex names
        names_list = [network(a) if 'networks' in a[0] else atlas(a) for a in dic['names'].reshape(164, ).tolist()]
        np.nan_to_num(matz, 0)
	    # getting fishers inverse and correspondant threshold
        inverse = inverseApplication(matz.copy())
        fishersThreshold = 0.5 * np.log((1 + threshold) / (1 - threshold))
        edgeWeights_fisher = createGraph("./graphs/graph_{}_fisher".format(name_subject), matz, fishersThreshold, [])
        edgeWeights = createGraph("./graphs/graph_{}".format(name_subject), inverse, threshold, [])
        graph_fisher = igraph.Graph.Read_Pajek("./graphs/graph_{}_fisher.net".format(name_subject))
        graph = igraph.Graph.Read_Pajek("./graphs/graph_{}.net".format(name_subject))
        res_fisher = calculatingValuesWithWeights(graph_fisher, edgeWeights_fisher, len(matz))
        res_normal = calculatingValuesWithWeights(graph, edgeWeights, len(matz))
        res_dic["{}_fisher".format(name_subject)] = res_fisher
        res_dic["{}_normal".format(name_subject)] = res_normal
        end = time.time()
        print('time for subject {}: {}'.format(name_subject, end - start))
        return res_dic
    #change n_jobs to specify another parallelization
    res = Parallel(n_jobs=3)(delayed(calc_data)(dir,res_dic,threshold) for dir in dirs)
    
    for i in range(0,len(res)):
        res_dic.update(res[i])
  
    with open("./results/{}.txt".format(name), 'w') as file:
        file.write(json.dumps(res_dic,default=converter,sort_keys=True, indent=1, separators= (',',':')))

    dict_file = open("./results/{}.pkl".format(name), "wb")
    pickle.dump(res_dic, dict_file)
    dict_file.close()
    return res_dic







